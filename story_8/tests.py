from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import index

# Create your tests here.


class Story8UnitTest(TestCase):
    def test_story8_url_is_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_story8_url_is_not_exist(self):
        response = Client().get('/error/')
        self.assertEqual(response.status_code, 404)

    def test_story8_contains_about(self):
        response = self.client.get('/home/')
        self.assertContains(response, 'About')

    def test_story_using_index_template(self):
    	response = Client().get('/home/')
    	self.assertTemplateUsed(response, 'index.html')

    def test_root_url_resolves_to_home(self):
        found = resolve('/home/')
        self.assertEqual(found.func, index)

# Unit test for Story 9

    def test_story9_url_is_exist(self):
    	response = Client().get('/books/')
    	self.assertEqual(response.status_code, 200)

    def test_story9_using_buku_template(self):
    	response = Client().get('/books/')
    	self.assertTemplateUsed(response, 'buku.html')

    def test_story9_content_book_title(self):
    	response = Client().get('/books/')
    	self.assertContains(response, "Title")

class Story8FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = webdriver.chrome.options.Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(Story8FunctionalTest, self).setUp()
        

    def tearDown(self):
        self.selenium.quit()
        super(Story8FunctionalTest, self).tearDown()

# # Local Test
    # def test_add_favorite_local(self):
    #     selenium = self.selenium
    #     # Opening the link we want to test
    #     selenium.get('http://127.0.0.1:8000/books/')

    #     favCount = selenium.find_element_by_id('FavoriteCount')

    #     click = selenium.find_element_by_class_name("favorite")

    #     # Fill the form with data

    #     # submitting the form
        # click.send_keys(Keys.RETURN)

        # check the returned result
        # self.assertEqual('0', favCount)

#     def test_element_layout_html_local(self):
#     	selenium = self.selenium

#     	selenium.get('http://127.0.0.1:8000')

#     	headtext = selenium.find_element_by_tag_name('h1').text
#     	self.assertEqual(headtext, 'Hello, Apa Kabar?')

#     def test_element_layout_html2_local(self):
#     	selenium = self.selenium
#     	selenium.get('http://127.0.0.1:8000')

#     	my_status = selenium.find_element_by_tag_name('h3').text
#     	self.assertEqual(my_status, 'My Status')

#     def test_element_css_local(self):
#     	selenium = self.selenium
#     	selenium.get('http://127.0.0.1:8000')

#     	judul = selenium.find_element_by_id('judul').value_of_css_property('text-align')
#     	self.assertIn('center', judul)

#     def test_element_css2_local(self):
#     	selenium = self.selenium
#     	selenium.get('http://127.0.0.1:8000')

#     	judulcolor = selenium.find_element_by_id('judul').value_of_css_property('color')
#     	self.assertIn('rgba(173, 216, 230, 1)', judulcolor)


#Heroku Test
    # def test_input_status(self):
    #     selenium = self.selenium
    #     # Opening the link we want to test
    #     selenium.get('https://usman-story6.herokuapp.com')
    #     # find the form element
    #     status = selenium.find_element_by_id('id_status_form')

    #     submit = selenium.find_element_by_id('submit')

    #     # Fill the form with data
    #     status.send_keys('Coba Coba')

    #     # submitting the form
    #     submit.send_keys(Keys.RETURN)

    #     # check the returned result
    #     self.assertIn('Coba Coba', selenium.page_source)

    # def test_element_layout_html(self):
    # 	selenium = self.selenium

    # 	selenium.get('https://usman-story6.herokuapp.com')

    # 	headtext = selenium.find_element_by_tag_name('h1').text
    # 	self.assertEqual(headtext, 'Hello, Apa Kabar?')

    # def test_element_layout_html2(self):
    # 	selenium = self.selenium
    # 	selenium.get('https://usman-story6.herokuapp.com')

    # 	my_status = selenium.find_element_by_tag_name('h3').text
    # 	self.assertEqual(my_status, 'My Status')

    # def test_element_css(self):
    # 	selenium = self.selenium
    # 	selenium.get('https://usman-story6.herokuapp.com')

    # 	judul = selenium.find_element_by_id('judul').value_of_css_property('text-align')
    # 	self.assertIn('center', judul)

    # def test_element_css2(self):
    # 	selenium = self.selenium
    # 	selenium.get('https://usman-story6.herokuapp.com')

    # 	judulcolor = selenium.find_element_by_id('judul').value_of_css_property('color')
    # 	self.assertIn('rgba(173, 216, 230, 1)', judulcolor)


