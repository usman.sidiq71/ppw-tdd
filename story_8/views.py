from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
import urllib.request, json
import requests


response = {}
def index(request):
	response['author'] = "Usman Sidiq"
	html = 'index.html'
	return render(request, html, response)
# Create your views here.


def books(request):
	return render(request, 'buku.html')

def book_list(request):
	book_api = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
	return JsonResponse(book_api)
