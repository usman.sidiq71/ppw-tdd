from django.urls import path
from .views import index, books, book_list

urlpatterns = [
	path('', index, name='home'),
	path('home/', index, name = 'home'),
	path('books/', books, name = 'books'),
	path('book_list/', book_list, name = 'book_list')
]