$(document).ready(function() {
  $('#accordion').accordion({
    active: false,
    collapsible: true            
});
  console.log("disini");
   $.ajax({
        url : "/book_list/",
        dataType : "json",
        success : function (datajson) {
        	console.log("masuk");
            var book_list = datajson.items;
            for (var i = 0; i < book_list.length; i++) {
                var table_content = "<tr>" +
                    "<td>" + '<img src="'+ book_list[i].volumeInfo.imageLinks.smallThumbnail +'" class="img-fluid rounded mx-auto d-block" alt="'+book_list[i].volumeInfo.previewLinks+'">' + "</td>" +
                    "<th scope=\"row\">" + book_list[i].volumeInfo.title + "</th>" +
                    "<td>" + book_list[i].volumeInfo.publisher + "</td>" +
                    "<td class=\"favorite star-symbol\"><a href='#'>☆</a></td>" +
                "</tr>";
                $('#contentTable').append(table_content);
            };
        },
        
        error: function (error) {
            var table_content = "<tr><td colspan=\"5\" class = \"text-center\">JSON failed to loaded :(</td></tr>";
            $('#contentTable').append(table_content);
        },
    });

    var count_favorite = 0;
    update_favorite();
    function update_favorite() {
        $('#FavoriteCount').text(count_favorite);
    };

    $(document).on('click', '.favorite', function(){
        if($(this).text() == "☆"){
            count_favorite++;
            update_favorite();
            $(this).text("★");
        }
        else{
            count_favorite--;
            update_favorite();
            $(this).text("☆");
        }
    });

});

var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 1000);
}

function showPage() {
  $("#loader").hide();
  document.getElementById("profile-content").style.visibility = "visible";
}

function changeTheme() {
	var checkbox = document.getElementById("theme-checkbox01");

	if(checkbox.checked == true){
		$("#home-flex").css("background-color","#787878");
		$("#home-flex").css("color","#F5F5F5");

		$("#about").css("background-color","#C0C0C0");

		$("#education").css("background-color","#F5F5F5");

		$("#experience").css("background-color","#C0C0C0");

		$("#more-info").css("background-color","#F5F5F5");

		$("#contact").css("background-color","#181818");

		$("#accordion .ui-accordion-header").css("background-color","#181818");

		$(".container-fluid").css("background-color","#181818");
	}
	else{
		$("#home-flex").css("background-color","#73C942");
		$("#home-flex").css("color","#000000");

		$("#about").css("background-color","#BDEDA1");

		$("#education").css("background-color","#E3F9D6");

		$("#experience").css("background-color","#BDEDA1");

		$("#more-info").css("background-color","#E3F9D6");

		$("#contact").css("background-color","#162D09");

		$("#accordion .ui-accordion-header").css("background-color","#162D09");

		$(".container-fluid").css("background-color","#162D09");

	}
}