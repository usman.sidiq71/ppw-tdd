# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from django.http import HttpRequest
# from .views import index
# from .models import Status
# from .forms import Status_Form

# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options

# # Create your tests here.


# class Story6UnitTest(TestCase):
#     def test_story6_url_is_exist(self):
#         response = Client().get('/home/')
#         self.assertEqual(response.status_code, 200)

#     def test_story6_url_is_not_exist(self):
#         response = Client().get('/error/')
#         self.assertEqual(response.status_code, 404)

#     def test_story6_contains_hello(self):
#         response = self.client.get('/home/')
#         self.assertContains(response, 'Hello, Apa Kabar?')

#     def test_story6_using_landingpage_template(self):
#     	response = Client().get('/home/')
#     	self.assertTemplateUsed(response, 'landingpage.html')

#     def test_root_url_resolves_to_home(self):
#         found = resolve('/home/')
#         self.assertEqual(found.func, index)

#     def test_model_can_add_new_status(self):
#         new_status = Status.objects.create(status_form = "Coba Coba")
#         counting_all_status = Status.objects.all().count()
#         self.assertEqual(counting_all_status, 1)

#     def test_form_add_status_has_placeholder(self):
#         form = Status_Form()
#         self.assertIn('class="status_form-textarea', form.as_p())
#         self.assertIn('id="id_status_form', form.as_p())
#         self.assertIn('type="text', form.as_p())

#     def test_model_can_delete_status(self):
#         new_status = Status.objects.create(status_form = "Coba Coba")
#         delete_status = Status.objects.all().delete()
#         counting_all_status = Status.objects.all().count()
#         self.assertEqual(counting_all_status, 0)

#     def test_form_error_message_for_blank_field(self):
#         form = Status_Form(data={'Status_Form': ''})
#         self.assertFalse(form.is_valid())
#         self.assertEqual(form.errors['status_form'], ["This field is required."])
    
#     def test_story6_post_success_and_render_the_result(self):
#         response_post = Client().post('/add_status/', {'status_form' : 'test'})
#         self.assertEqual(response_post.status_code, 302)

#         response = Client().get('/home/')
#         html_response = response.content.decode('utf8')
#         self.assertIn('test', html_response)

#     def test_story6_post_error_and_render_the_result(self):
#         test = 'Anonymous'
#         response_post = Client().post('/add_status/', {'status_form': ''})
#         self.assertEqual(response_post.status_code, 302)

#         response = Client().get('/home/')
#         html_response = response.content.decode('utf8')
#         self.assertNotIn(test, html_response)

# class Story6FunctionalTest(TestCase):
#     def setUp(self):
#         chrome_options = webdriver.chrome.options.Options()
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--disable-dev-shm-usage')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium = webdriver.Chrome(chrome_options=chrome_options)
#         self.selenium.implicitly_wait(25)
#         super(Story6FunctionalTest, self).setUp()
        

#     def tearDown(self):
#         self.selenium.quit()
#         super(Story6FunctionalTest, self).tearDown()

# # Local Test
#     def test_input_status_local(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         selenium.get('http://127.0.0.1:8000')
#         # find the form element
#         status = selenium.find_element_by_id('id_status_form')

#         submit = selenium.find_element_by_id('submit')

#         # Fill the form with data
#         status.send_keys('Coba Coba')

#         # submitting the form
#         submit.send_keys(Keys.RETURN)

#         # check the returned result
#         self.assertIn('Coba Coba', selenium.page_source)

#     def test_element_layout_html_local(self):
#     	selenium = self.selenium

#     	selenium.get('http://127.0.0.1:8000')

#     	headtext = selenium.find_element_by_tag_name('h1').text
#     	self.assertEqual(headtext, 'Hello, Apa Kabar?')

#     def test_element_layout_html2_local(self):
#     	selenium = self.selenium
#     	selenium.get('http://127.0.0.1:8000')

#     	my_status = selenium.find_element_by_tag_name('h3').text
#     	self.assertEqual(my_status, 'My Status')

#     def test_element_css_local(self):
#     	selenium = self.selenium
#     	selenium.get('http://127.0.0.1:8000')

#     	judul = selenium.find_element_by_id('judul').value_of_css_property('text-align')
#     	self.assertIn('center', judul)

#     def test_element_css2_local(self):
#     	selenium = self.selenium
#     	selenium.get('http://127.0.0.1:8000')

#     	judulcolor = selenium.find_element_by_id('judul').value_of_css_property('color')
#     	self.assertIn('rgba(173, 216, 230, 1)', judulcolor)


# #Heroku Test
#     def test_input_status(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         selenium.get('https://usman-story6.herokuapp.com')
#         # find the form element
#         status = selenium.find_element_by_id('id_status_form')

#         submit = selenium.find_element_by_id('submit')

#         # Fill the form with data
#         status.send_keys('Coba Coba')

#         # submitting the form
#         submit.send_keys(Keys.RETURN)

#         # check the returned result
#         self.assertIn('Coba Coba', selenium.page_source)

#     def test_element_layout_html(self):
#     	selenium = self.selenium

#     	selenium.get('https://usman-story6.herokuapp.com')

#     	headtext = selenium.find_element_by_tag_name('h1').text
#     	self.assertEqual(headtext, 'Hello, Apa Kabar?')

#     def test_element_layout_html2(self):
#     	selenium = self.selenium
#     	selenium.get('https://usman-story6.herokuapp.com')

#     	my_status = selenium.find_element_by_tag_name('h3').text
#     	self.assertEqual(my_status, 'My Status')

#     def test_element_css(self):
#     	selenium = self.selenium
#     	selenium.get('https://usman-story6.herokuapp.com')

#     	judul = selenium.find_element_by_id('judul').value_of_css_property('text-align')
#     	self.assertIn('center', judul)

#     def test_element_css2(self):
#     	selenium = self.selenium
#     	selenium.get('https://usman-story6.herokuapp.com')

#     	judulcolor = selenium.find_element_by_id('judul').value_of_css_property('color')
#     	self.assertIn('rgba(173, 216, 230, 1)', judulcolor)


