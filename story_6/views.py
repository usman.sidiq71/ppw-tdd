from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from .forms import Status_Form
from .models import Status
# Create your views here.
response = {}
def index(request):
	response['author'] = "Usman Sidiq"
	status = Status.objects.all()
	response['status'] = status
	html = 'landingpage.html'
	response['status_form'] = Status_Form
	return render(request, html, response)

def add_status(request):
	form = Status_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['status_form'] = request.POST['status_form']
		status_list = Status(status_form = response['status_form'])
		status_list.save()
		return HttpResponseRedirect('/home/')
	else:
		return HttpResponseRedirect('/home/')

def del_status(request):
	response['author'] = "Usman Sidiq"
	status = Status.objects.all().delete()
	response['status'] = status
	html = 'landingpage.html'
	response['status_form'] = Status_Form
	return render(request, html, response)