from django.urls import path
from .views import index, add_status, del_status

urlpatterns = [
	path('', index, name='home'),
	path('home/', index, name = 'home'),
	path('add_status/', add_status, name='add_status'),
	path('delete/', del_status, name='delete'),
]