from django import forms

class Status_Form(forms.Form):
	error_messages = {
		'required' : 'Silahkan mengisi input di sini'
	}
	status_attrs = {
		'type' : 'text',
		'class' : 'status_form-textarea',
		'placeholder' : 'Apa status anda hari ini?'
	}

	status_form = forms.CharField(
		label='', required=True,
		max_length = 300,
		widget=forms.Textarea(attrs=status_attrs)
		)
